variable "prefix" {
  description = "A prefix used for all resources in this example"
}

variable "location" {
  default = "Central US"
  description = "The Azure Region in which all resources in this example should be provisioned"
}

variable "resourceID" {
  default = "1-e8733c77-playground-sandbox"
  description = "The Azure Resource group ID (existing)"
}
